# Secants and Tangents #

### What is this repository for? ###

This code demonstrates various ways to approximate a nonlinear function in a mixed integer linear program.

The underlying problem is to select discrete order quantities for multiple products, given unit price (which is actually irrelevant), demand, holding cost and setup cost information for each, along with the storage volume per unit for each. In isolation, one might compute the [economic order quantity](https://en.wikipedia.org/wiki/Economic_order_quantity) (EOQ) for each, round to the nearest integer, and order that much each time. Here we assume limited storage capacity and require that the sum of the average storage volumes (volume based on half the order quantity of each) cannot exceed the available capacity. The objective is to select order quantities that minimize total cost.

The nonlinearity in the problem arises from the reciprocal relationship between order quantity and order frequency. The code demonstrates four different models for solving the problem, approximating order frequency as:

* a piecewise-linear function of order quantity, using secants;
* a piecewise-linear function of order quantity, using tangents;
* the minimum of a set of linear functions computed from tangents; and
* the minimum of a set of linear functions computed from tangents with additional tangents computed in a callback when a solution underestimates order frequency.

The various approximations are explained and compared in two blog posts: [Approximating Nonlinear Functions: Tangents v. Secants](https://orinanobworld.blogspot.com/2020/03/approximating-nonlinear-functions.html) and [Tangents v. Secants Part II](https://orinanobworld.blogspot.com/2020/04/tangents-v-secants-part-ii.html).

The code was developed using CPLEX 12.10 but will run with at least some earlier versions.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

