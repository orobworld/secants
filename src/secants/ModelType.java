package secants;

/**
 * ModelType enumerates ways to approximate the nonlinear function.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public enum ModelType {
  /** Use tangents to approximate the function. */
  TANGENTS("tangents"),
  /** Use secants/chords to approximate the function. */
  SECANTS("secants");

  private final String label;

  /**
   * Constructor.
   * @param name the label to use
   */
  ModelType(final String name) {
    label = name;
  }

  /**
   * Generates a label for the value.
   * @return the label
   */
  @Override
  public final String toString() {
    return label;
  }
}
