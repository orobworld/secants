package secants;

import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import java.util.Random;

/**
 * Secants demonstrates the use of secants and tangents to approximate a
 * nonlinear function in a MILP model.
 *
 * The example used is a multiple product EOQ model with limited storage space
 * and discrete order quantities. The objective is to minimize annual inventory
 * cost.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Secants {

  /**
   * Dummy constructor.
   */
  private Secants() { }

  /**
   * Runs the demonstration.
   * @param args the command line arguments
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Set some parameters.
    long seed = 456;        // random number seed
    int nProducts = 10;     // number of products
    double capRatio = 0.7;  // assume enough space to hold about this much
                            // average inventory using individual EOQs
    int gridSize = 10;      // number of grid points for approximations
    // Generate a test problem.
    Random rng = new Random(seed);
    Problem problem = new Problem(nProducts, capRatio, rng);
    // Show the user the problem.
    System.out.println(problem);
    // Create and solve a MIP model using pwl functions from secants.
    System.out.println("\nSolving with pwl functions from secants ...\n");
    try (MIP mip = new SecantModel(problem, gridSize)) {
      IloCplex.Status status = mip.solve();
      System.out.println("Final MIP status = " + status);
      System.out.println(mip.report());
      // Save the solution (which is feasible but may be suboptimal) for
      // use later.
    } catch (IloException ex) {
      System.err.println("CPLEX throw an exception:\n" + ex.getMessage());
    }
    // Create and solve a MIP model using pwl functions from tangents.
    System.out.println("\nSolving with pwl functions from tangents ...\n");
    try (MIP mip = new TangentModel(problem, gridSize)) {
      IloCplex.Status status = mip.solve();
      System.out.println("Final MIP status = " + status);
      System.out.println(mip.report());
    } catch (IloException ex) {
      System.err.println("CPLEX throw an exception:\n" + ex.getMessage());
    }
    // Create and solve a MIP model using tangents.
    System.out.println("\nSolving with collections of tangents ...\n");
    try (MIP mip = new TangentModel2(problem, gridSize)) {
      IloCplex.Status status = mip.solve();
      System.out.println("Final MIP status = " + status);
      System.out.println(mip.report());
    } catch (IloException ex) {
      System.err.println("CPLEX throw an exception:\n" + ex.getMessage());
    }
    // Create and solve a model using tangents and a callback.
    System.out.println("\nSolving with tangents plus callback ...\n");
    try (LazyTangentModel mip = new LazyTangentModel(problem, gridSize)) {
      // Try to solve it.
      IloCplex.Status status = mip.solve();
      System.out.println("Final MIP status = " + status);
      System.out.println(mip.report());
      System.out.println("Total number of cuts generated = " + mip.getNCuts());
    } catch (IloException ex) {
      System.err.println("CPLEX threw an exception:\n" + ex.getMessage());
    }
  }

  /**
   * Formats a double.
   * @param d the double
   * @return the formatted double
   */
  public static String dfmt(final double d) {
    return String.format("%8.2f", d);
  }

  /**
   * Formats an integer.
   * @param i the integer
   * @return the formatted integer
   */
  public static String ifmt(final int i) {
    return String.format("%4d", i);
  }
}
