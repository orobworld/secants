package secants;

import java.util.Arrays;
import java.util.Random;

/**
 * Problem creates and stores a multiple EOQ problem, where products are ordered
 * in discrete amounts, and storage space is limited.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  // Ranges for model parameters.
  private static final double LODEMAND = 500;    // lower limit on item demand
  private static final double HIDEMAND = 2000;   // upper limit on item demand
  private static final double LOCOST = 1;        // lower limit on unit cost
  private static final double HICOST = 5;        // upper limit on unit cost
  private static final double LOORDER = 2;       // lower limit on order cost
  private static final double HIORDER = 4;       // upper limit on order cost
  private static final double LOSIZE = 1;        // lower limit on unit size
  private static final double HISIZE = 5;        // upper limit on unit size
  private static final double HOLDING = 0.2;     // holding rate (all products)

  private final int nProducts;            // number of products
  private final double[] unitCost;        // unit purchase costs
  private final double[] orderCost;       // order placement costs
  private final int[] demand;             // demand for each product
  private final double[] size;            // shelf space per unit
  private final double space;             // total shelf space

  /**
   * Constructor.
   * @param prods the number of products
   * @param ratio ratio of shelf space to space requirement if all products
   * use individual EOQs
   * @param rng a random number generator
   */
  public Problem(final int prods, final double ratio, final Random rng) {
    nProducts = prods;
    // Generate unit purchase costs, shelf space requirements and order
    // placement costs.
    unitCost = rng.doubles(LOCOST, HICOST).limit(nProducts).toArray();
    size = rng.doubles(LOSIZE, HISIZE).limit(nProducts).toArray();
    orderCost = rng.doubles(LOORDER, HIORDER).limit(nProducts).toArray();
    // Generate an integer demand for each product.
    demand = rng.doubles(LODEMAND, HIDEMAND).limit(nProducts)
                .mapToInt((d) -> (int) Math.floor(d)).toArray();
    // Compute the total space for all products at half their individualEOQ
    // levels (representing average inventory in stock).
    double temp = 0;
    for (int i = 0; i < nProducts; i++) {
      temp += size[i] / 2 * Math.sqrt(2 * demand[i] * orderCost[i] / HOLDING);
    }
    // Set the total shelf space to a proportion of what individual EOQs
    // would consume.
    space = ratio * temp;
  }

  /**
   * Generates a string summarizing the problem.
   * @return the problem summary
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Number of products = ").append(nProducts).append(".\n\n")
      .append("Product\t\tUnit Cost\tOrder Cost\tDemand\t    Size\n");
    for (int i = 0; i < nProducts; i++) {
      sb.append("   ").append(i).append("\t\t")
        .append(Secants.dfmt(unitCost[i]))
        .append("\t").append(Secants.dfmt(orderCost[i])).append("\t")
        .append(Secants.ifmt(demand[i])).append("\t")
        .append(Secants.dfmt(size[i])).append("\n");
    }
    sb.append("\nTotal shelf space = ").append(Secants.dfmt(space))
      .append("\n");
    sb.append("Holding rate: = ").append(Secants.dfmt(HOLDING)).append("\n");
    return sb.toString();
  }

  /**
   * Gets the number of products in the problem.
   * @return the number of products
   */
  public int getNProducts() {
    return nProducts;
  }

  /**
   * Gets the total shelf space.
   * @return the shelf space
   */
  public double getSpace() {
    return space;
  }

  /**
   * Gets the holding cost rate.
   * @return the holding cost rate
   */
  public double getHoldingRate() {
    return HOLDING;
  }

  /**
   * Gets the product demands.
   * @return the product demands
   */
  public int[] getDemand() {
    return Arrays.copyOf(demand, nProducts);
  }

  /**
   * Gets the unit costs for all products.
   * @return the unit costs
   */
  public double[] getUnitCosts() {
    return Arrays.copyOf(unitCost, nProducts);
  }

  /**
   * Gets the order placement costs for all products.
   * @return the order placement cots
   */
  public double[] getOrderCosts() {
    return Arrays.copyOf(orderCost, nProducts);
  }

  /**
   * Gets the unit shelf space consumption for all products.
   * @return the shelf sizes
   */
  public double[] getSizes() {
    return Arrays.copyOf(size, nProducts);
  }

}
