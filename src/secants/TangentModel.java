package secants;

import ilog.concert.IloException;
import java.util.ArrayList;

/**
 * TangentModel uses piecewise-linear functions based on tangents to relate
 * order frequencies to order quantities.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class TangentModel extends MIP {

  /**
   * Constructor.
   * @param prob the problem to solve
   * @param nSteps the number of tangents to calculate
   * @throws IloException if model construction fails
   */
  public TangentModel(final Problem prob, final int nSteps)
         throws IloException {
    super(prob);
    int[] demand = problem.getDemand();
    // Constrain each order frequency to equal a pwl function of the order
    // quantity.
    for (int i = 0; i < nProducts; i++) {
      // Compute the order quantities at which tangents will be calculated.
      int stepSize = Math.floorDiv(demand[i], nSteps);
      ArrayList<Double> temp = new ArrayList<>();
      for (int j = 1; j <= nSteps; j++) {
        double z = j * stepSize + 1.0;
        if (z < demand[i]) {
          temp.add(z);
        }
      }
      double[] tpt = temp.stream().mapToDouble(z -> z).toArray();
      // Compute actual frequencies and tangent slopes at those points.
      double dmd = demand[i];
      double[] q = temp.stream().mapToDouble(z -> dmd / z).toArray();
      double[] s = temp.stream().mapToDouble(z -> -dmd / (z * z)).toArray();
      // Compute the points at which consecutive tangents intersect.
      int n = tpt.length - 1;
      double[] x = new double[n];
      double[] y = new double[n];
      for (int j = 0; j < n; j++) {
        // Find the point where the j-th and (j+1)-th tangents intersect.
        x[j] = (s[j + 1] * tpt[j + 1] - s[j] * tpt[j] + q[j] - q[j + 1])
               / (s[j + 1] - s[j]);
        y[j] = s[j] * (x[j] - tpt[j]) + q[j];
      }
      // Set quantity equal to a pwl function of frequency.
      mip.addEq(mip.piecewiseLinear(qty[i], s[0], x, y, s[n]), freq[i],
                "Reciprocal_" + i);
    }
  }
}
