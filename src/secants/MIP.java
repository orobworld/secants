package secants;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 * MIP is the base class for mixed-integer programming models for the EOQ
 * problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public class MIP implements AutoCloseable {
  protected final Problem problem;    // the problem to be solved
  protected final int nProducts;      // the number of products

  // CPLEX objects
  protected final IloCplex mip;         // the MIP model
  protected final IloNumVar[] freq;     // the frequency of orders
  protected final IloIntVar[] qty;      // the order amount for each product
  protected final IloNumVar spaceUsed;  // estimated space consumption

  /**
   * Constructor.
   * Child classes need to add constraints tying order frequencies to
   * order quantities.
   * @param prob the problem to solve
   * @throws IloException if any step in model construction fails
   */
  public MIP(final Problem prob) throws IloException {
    problem = prob;
    nProducts = problem.getNProducts();
    int[] demand = problem.getDemand();
    double[] unitCost = problem.getUnitCosts();
    double[] orderCost = problem.getOrderCosts();
    double[] unitSpace = problem.getSizes();
    double hold = problem.getHoldingRate() / 2.0;
    // Create the model object.
    mip = new IloCplex();
    // Create purchase frequency variables.
    freq = new IloNumVar[nProducts];
    for (int i = 0; i < nProducts; i++) {
      freq[i] = mip.numVar(1, demand[i], "freq_" + i);
    }
    // Create variables for order quantities.
    qty = new IloIntVar[nProducts];
    for (int i = 0; i < nProducts; i++) {
      qty[i] = mip.intVar(1, demand[i], "qty_" + i);
    }
    // Create a variable for space usage. Enforce the limit on available space
    // as a bound on it.
    spaceUsed = mip.numVar(0, problem.getSpace(), "spaceUsed");
    // Set the objective to minimize annual variable costs
    // (excludes purchase costs).
    IloLinearNumExpr totalCost = mip.linearNumExpr();
    for (int i = 0; i < nProducts; i++) {
      totalCost.addTerm(hold * unitCost[i], qty[i]);  // holding cost
      totalCost.addTerm(orderCost[i], freq[i]);       // order placement cost
    }
    mip.addMinimize(totalCost);
    // Constraint shelf space. The space occupied by the average inventory
    // levels of all products cannot exceed available space.
    IloLinearNumExpr totalSpace = mip.linearNumExpr();
    for (int i = 0; i < nProducts; i++) {
      totalSpace.addTerm(unitSpace[i] / 2.0, qty[i]);
    }
    mip.addEq(totalSpace, spaceUsed, "Shelf space");
  }

  /**
   * Closes the object.
   * @throws IloException if CPLEX cannot close successfully
   */
  @Override
  public final void close() throws IloException {
    mip.end();
  }

  /**
   * Solves the model.
   * @return the final model status
   * @throws IloException if anything goes wrong
   */
  public final IloCplex.Status solve() throws IloException {
    mip.solve();
    return mip.getStatus();
  }

  /**
   * Generates a solution report.
   * @return a string reporting on the problem solution
   * @throws IloException if recovering the solution blows up.
   */
  public final String report() throws IloException {
    double[] unitCost = problem.getUnitCosts();
    double[] orderCost = problem.getOrderCosts();
    double[] unitSpace = problem.getSizes();
    double hold = problem.getHoldingRate() / 2.0;
    int[] demand = problem.getDemand();
    // Make sure there is a solution.
    IloCplex.Status status = mip.getStatus();
    if (status == IloCplex.Status.Optimal
        || status == IloCplex.Status.Feasible) {
      StringBuilder sb = new StringBuilder();
      // Report the objective value.
      sb.append("\nOptimal cost = ").append(Secants.dfmt(mip.getObjValue()))
        .append("\n");
      // Report the order quantities and nominal and actual frequencies.
      double[] f = mip.getValues(freq);
      double[] q = mip.getValues(qty);
      double[] f0 = new double[nProducts];
      sb.append("\nProduct\tDemand\tQuantity\tActual Freq\tNominal Freq\n");
      for (int i = 0; i < demand.length; i++) {
        f0[i] = demand[i] / q[i];
        sb.append("   ").append(i).append("\t").append(Secants.ifmt(demand[i]))
          .append("\t  ").append(Secants.ifmt((int) q[i])).append("\t\t")
          .append(Secants.dfmt(f0[i])).append("\t")
          .append(Secants.dfmt(f[i])).append("\n");
      }
      // Calculate actual cost.
      double realCost = 0;
      for (int i = 0; i < nProducts; i++) {
        realCost += orderCost[i] * f0[i] + hold * unitCost[i] * q[i];
      }
      sb.append("\nActual total cost = ").append(Secants.dfmt(realCost))
        .append("\n");
      return sb.toString();
    } else {
      return "No solution found! Status = " + status + ".";
    }
  }

  /**
   * Exports the model (for debugging purposes).
   * @param where the name/path for the output file
   * @throws IloException if export fails
   */
  public final void export(final String where) throws IloException {
    mip.exportModel(where);
  }

  /**
   * Gets the order quantities in the solution.
   * @return the order quantities
   * @throws IloException if the solution cannot be recovered
   */
  public final double[] getQuantities() throws IloException {
    return mip.getValues(qty);
  }

  /**
   * Sets a starting solution for the model.
   * This is used for debugging.
   * @param quantity the order quantities in the starting solution
   * @throws IloException if the solution cannot be set
   */
  public final void setSolution(final double[] quantity) throws IloException {
    // Calculate the actual order frequencies.
    int[] demand = problem.getDemand();
    double[] f = new double[nProducts];
    for (int i = 0; i < nProducts; i++) {
      f[i] = demand[i] / quantity[i];
    }
    // Merge the quantity and frequency variables into a single vector,
    // and do the same for their values.
    IloNumVar[] vars = new IloNumVar[2 * nProducts];
    double[] vals = new double[2 * nProducts];
    for (int i = 0; i < nProducts; i++) {
      vars[i] = qty[i];
      vals[i] = quantity[i];
      vars[i + nProducts] = freq[i];
      vals[i + nProducts] = f[i];
    }
    // Set the MIP start.
    mip.addMIPStart(vars, vals, IloCplex.MIPStartEffort.Auto);
  }
}
