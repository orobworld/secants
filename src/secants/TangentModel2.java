package secants;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import java.util.ArrayList;

/**
 * TangentModel2 connects order frequencies to order quantities by a collection
 * of linear inequality constraints based on tangents. This is possible
 * because order frequency is a convex function of order quantity.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public class TangentModel2 extends MIP {

  /**
   * Constructor.
   * @param prob the problem to solve
   * @param nSteps the number of tangents to calculate
   * @throws IloException if model construction fails
   */
  public TangentModel2(final Problem prob, final int nSteps)
         throws IloException {
    super(prob);
    int[] demand = problem.getDemand();
    // For each product, generate a sequence of inequalities of the form
    // order frequency >= f(order quantity) where f() is a linear function
    // computed from a tangent to the actual function.
    for (int i = 0; i < nProducts; i++) {
      // Compute the order quantities at which tangents will be calculated.
      int stepSize = Math.floorDiv(demand[i], nSteps);
      ArrayList<Double> temp = new ArrayList<>();
      for (int j = 1; j <= nSteps; j++) {
        double z = j * stepSize + 1.0;
        if (z < demand[i]) {
          temp.add(z);
        }
      }
      double[] x = temp.stream().mapToDouble(z -> z).toArray();
      // For each tangent, add a constraint to the model.
      for (int j = 0; j < x.length; j++) {
        mip.add(makeConstraint(qty[i], freq[i], x[j], demand[i],
                               "Tangent_" + i + "_" + j));
      }
    }
  }

  /**
   * Generates a constraint from a tangent.
   * @param quantity the order quantity variable
   * @param frequency the order frequency variable
   * @param x the order quantity at which the tangent is calculated
   * @param demand the annual demand for the item
   * @param name a name to give the constraint
   * @return the generated constraint
   * @throws IloException if constraint generation fails
   */
  protected final IloRange makeConstraint(final IloIntVar quantity,
                                          final IloNumVar frequency,
                                          final double x, final double demand,
                                          final String name)
                     throws IloException {
    double y = demand / x;
    double s = -demand / (x * x);
    IloLinearNumExpr expr = mip.linearNumExpr(y - s * x);
    expr.addTerm(s, quantity);
    if (name == null) {
      return (IloRange) mip.ge(frequency, expr);
    } else {
      return (IloRange) mip.ge(frequency, expr, name);
    }
  }
}
