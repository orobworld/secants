package secants;

import ilog.concert.IloException;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import java.util.ArrayList;

/**
 * LazyTangents combines the tangents from TangentModel2 with a lazy constraint
 * that checks proposed solutions. If the proposed solution underestimates
 * any order frequency, the callback adds a lazy constraint based on the
 * tangents at that specific order quantity to tighten the model, and rejects
 * the solution.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class LazyTangentModel extends TangentModel2 {
  private final double tol = 0.01;  // rounding tolerance
  private final int[] demand;       // product demands
  private long nCuts;               // number of cuts generated

  /**
   * Constructor.
   * @param prob the problem to solve
   * @param nSteps the number of tangents to calculate
   * @throws IloException if model construction fails
   */
  public LazyTangentModel(final Problem prob, final int nSteps)
         throws IloException {
    super(prob, nSteps);
    demand = problem.getDemand();
    nCuts = 0;
    // Attach a generic callback that evaluations solutions and adds tangent
    // constraints as needed.
    mip.use(new LC(), IloCplex.Callback.Context.Id.Candidate);
  }

  /**
   * Gets the number of cuts generated.
   * @return the number of cuts
   */
  public long getNCuts() {
    return nCuts;
  }

  /**
   * Lazy constraint callback class.
   */
  private class LC implements IloCplex.Callback.Function {

    /**
     * Tests if a candidate solution is valid, meaning that no order frequency
     * is underestimated given the item's order quantity. If a frequency is
     * underestimated, the tangent at that quantity is used to generate a
     * linear inequality that is added lazily.
     * @param context the context from which the callback was called
     * @throws IloException if CPLEX blows up for some reason
     */
    @Override
    public void invoke(final IloCplex.Callback.Context context)
                       throws IloException {
      // Get the order frequencies and alleged order quantities.
      double[] f = context.getCandidatePoint(freq);
      double[] q = context.getCandidatePoint(qty);
      ArrayList<IloRange> cuts = new ArrayList<>();
      for (int i = 0; i < nProducts; i++) {
        if (f[i] < demand[i] / q[i] - tol) {
          // The frequency for this item is too low; add a cut.
          IloRange r = makeConstraint(qty[i], freq[i], q[i], demand[i], null);
          cuts.add(r);
        }
      }
      // If any cuts were generated, reject the candidate and add all of them.
      if (!cuts.isEmpty()) {
        nCuts += cuts.size();
        IloRange[] cuts2 = cuts.toArray(new IloRange[cuts.size()]);
        context.rejectCandidate(cuts2);
      }
    }
  }
}
