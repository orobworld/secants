package secants;

import ilog.concert.IloException;
import java.util.ArrayList;

/**
 * SecantModel uses piecewise-linear functions based on secants to relate
 * order frequencies to order quantities. Breakpoints are integer order
 * quantities, so that frequencies are exact at breakpoints.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class SecantModel extends MIP {

  /**
   * Constructor.
   * @param prob the problem to solve
   * @param nSteps the desired number of steps to use
   * @throws IloException if model formulation fails
   */
  SecantModel(final Problem prob, final int nSteps) throws IloException {
    super(prob);
    // Add pwl functions approximating the relationship
    // quantity = demand / order frequency.
    int[] demand = problem.getDemand();
    for (int i = 0; i < nProducts; i++) {
      // Set an integer step size to get approximately the desired number of
      // breakpoints.
      int stepSize = Math.floorDiv(demand[i], nSteps);
      ArrayList<Double> temp = new ArrayList<>();
      for (int j = 1; j <= nSteps; j++) {
        double z = j * stepSize + 1.0;
        if (z < demand[i]) {
          temp.add(z);
        }
      }
      // Compute the breakpoint abscissas and ordinates.
      double[] x = temp.stream().mapToDouble(z -> z).toArray();
      double[] y = new double[x.length];
      for (int j = 0; j < x.length; j++) {
        y[j] = demand[i] / x[j];
      }
      // Compute the slopes of the initial and final segments.
      int m = x.length - 1;
      double slope1 = (demand[i] - y[0]) / (1 - x[0]);
      double slope2 = (1.0 - y[m]) / (demand[i] - x[m]);
      // Constraint frequency to (approximately) equal demand divided by order
      // quantity.
      mip.addEq(mip.piecewiseLinear(qty[i], slope1, x, y, slope2), freq[i],
                "Reciprocal_" + i);
    }
  }
}
